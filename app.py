"""
File                : performance_tracker_module.py

Description         : This file contains the starting method for Furnace
                      Performance Tracker Algorithm. When this module is
                      started,
                      it schedules the algorithm to run once in every
                      configured interval

Author              : LNS Team

Date Created        : 06-Dec-2019

Date Last modified  :

Copyright (C) 2019 LivNSense Technologies - All Rights Reserved

"""

import time
import pandas as pd
import schedule
# from FurnacePTConfig import SCHEDULING_INTERVAL
from performance_tracker_config import logger, CONFIG_PARAM_FILE_PATH

from performance_tracker_calculation import non_furnace_ppt_moduleStart
# from pylint import lint
'''
Scheduler class definition
'''

if __name__ == "__main__":
    """Create a scheduler object and schedule Furnace Performance Tracker
    Algorithm  to run at every  configured Interval """

    # Starting point of Furnace Performance Tracker Algorithm Module

    try:
        while True:

            config_df = pd.read_csv(CONFIG_PARAM_FILE_PATH)
            config_df = config_df.set_index('Parameter')
            SCH_INT = config_df.loc['scheduling_interval', 'Value']
            for minute in range(2, 60, SCH_INT):
                val = ":{minute:02d}".format(minute=minute)
                schedule.every().hours.at(val).do(non_furnace_ppt_moduleStart)
            while True:
                config_df = pd.read_csv(CONFIG_PARAM_FILE_PATH)
                config_df = config_df.set_index('Parameter')
                SCHEDULE_INT = config_df.loc['scheduling_interval', 'Value']
                if not(SCH_INT == SCHEDULE_INT):
                    schedule.clear()
                    break
                schedule.run_pending()
                time.sleep(60)
    except Exception as ex:
        print("Failed to schedule a Furnace Performance Tracker Algorithm.",
              str(ex))
        logger.info("Failed to schedule a Furnace Performance Tracker "
                    "Algorithm...," + str(ex))
